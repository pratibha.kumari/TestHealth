package com.insurance.test;

import java.util.Map;

public class Customer {
	
		
		private String name;
		
		private String gender;
		
		private int age;
		
		private Map<String, Boolean> healthStatus;
		
		private Map<String, Boolean> habits;
		
		private double premium;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public Map<String, Boolean> getHealthStatus() {
			return healthStatus;
		}

		public void setHealthStatus(Map<String, Boolean> healthStatus) {
			this.healthStatus = healthStatus;
		}

		public Map<String, Boolean> getHabits() {
			return habits;
		}

		public void setHabits(Map<String, Boolean> habits) {
			this.habits = habits;
		}

		public double getPremium() {
			return premium;
		}

		public void setPremium(double premium) {
			this.premium = premium;
		}
	

}
