package com.insurance.test;

public class PremiumCalcDecorator implements PremiumCalculator{


		protected PremiumCalculator premiumCalc;
		
		public PremiumCalcDecorator(PremiumCalculator premiumCalc) {
			this.premiumCalc = premiumCalc;
		}
		
		@Override
		public Customer calculatePremium(Customer customer) {
			return this.premiumCalc.calculatePremium(customer);
		}

	

}
