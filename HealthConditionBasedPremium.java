package com.insurance.test;
import java.util.Iterator;
import java.util.Set;

public class HealthConditionBasedPremium extends PremiumCalcDecorator{
	
public HealthConditionBasedPremium(PremiumCalculator premiumCalc) {
			super(premiumCalc);
		}
		
		@Override
		public Customer calculatePremium(Customer customer) {
			super.calculatePremium(customer);
			System.out.println("Health Condition Based premium...");
			Set<String> healthCond = customer.getHealthStatus().keySet();
			Iterator<String> healthItr = healthCond.iterator();
			String healthCondName = "";
			while(healthItr.hasNext()){
				healthCondName = healthItr.next();
				if(customer.getHealthStatus().get(healthCondName)){
					customer.setPremium(customer.getPremium() + (customer.getPremium() * 1/100));
				}
			}
			return customer;
		}
	

}
