package com.insurance.test;

public class GenderBasedPremium extends PremiumCalcDecorator{
	

		public GenderBasedPremium(PremiumCalculator premiumCalc) {
			super(premiumCalc);
		}
		
		@Override
		public Customer calculatePremium(Customer customer) {
			super.calculatePremium(customer);
			System.out.println("Gender Based premium..." + customer.getPremium());
			switch (customer.getGender()) {
			case "MALE":
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 2/100));
				break;
			case "FEMALE":
			case "OTHER":
			default:
				break;
			}
			return customer;
		}

	

}
