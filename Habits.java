package com.insurance.test;

public enum Habits {
	BAD_HABIT_SMOKING,
	BAD_HABIT_ALCOHOL,
	GOOD_HABIT_DAILY_EXERCISE,
	BAD_HABIT_DRUGS

}
