package com.insurance.test;

public class AgeBasedPremium extends PremiumCalcDecorator{
	

		public AgeBasedPremium(PremiumCalculator premiumCalc) {
			super(premiumCalc);
		}
		
		@Override
		public Customer calculatePremium(Customer customer) {
			super.calculatePremium(customer);
			System.out.println("Age Based premium...");
			if(customer.getAge() >= 18 && customer.getAge() <= 40)
			{
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 10/100));
			}
			else if(customer.getAge() > 40)
			{
				customer.setPremium(customer.getPremium() + (customer.getPremium() * 20/100));
			}
			return customer;
		}

	

}
