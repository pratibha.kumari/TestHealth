package com.insurance.test;

public class BasicPremium implements PremiumCalculator{

	private static final double BASIC_PREMIUM = 5000;
		
		@Override
		public Customer calculatePremium(Customer customer) {
			System.out.println("Basic premium....");
			customer.setPremium(BASIC_PREMIUM);
			return customer;
		}

	
}
